require 'spec_helper'
require 'byebug'

describe Dropigee do
  let(:client_id) { 'client_id_here' }
  let(:client_secret) { 'client_secret_here' }
  let(:super_cell_name) { 'sc_name_here'}
  let(:drop_id) { 'drop_id_of_sc_above' }

  let(:file_path) { File.join File.dirname(__FILE__), "/fixtures/dropigee.svg" }
  let(:file) { File.new file_path }

  let(:options) {{
      client_id: client_id,
      client_secret: client_secret,
      scope: 'write:drop',
      super_cell_name: super_cell_name,
      grant_type: 'client_credentials'
    }
  }

  subject { described_class.new options }


  it 'has a version number' do
    expect(Dropigee::VERSION).not_to be nil
  end

  describe '#initialize' do
    describe 'options' do
      shared_examples_for 'setted option' do |name|
        it "sets #{name} option" do
          expect(subject.get_options).to include name
          expect(subject.get_options[name]).to eq options[name]
        end
      end

      shared_examples_for 'validates option' do |name|
        let(:incorrect_options) { options.merge({name => ''}) }

        it "fails without #{name}" do
          expect{ described_class.new(incorrect_options) }.to raise_error ArgumentError
        end
      end

      describe 'client_id' do
        it_behaves_like 'setted option', :client_id

        it_behaves_like 'validates option', :client_id
      end

      describe 'client_secret' do
        it_behaves_like 'setted option', :client_secret

        context 'with incorrect client_secret' do
          let(:incorrect_options) { options.merge({client_secret: ''}) }

          it_behaves_like 'validates option', :client_secret
        end
      end

      describe 'super_cell_name' do
        it_behaves_like 'setted option', :super_cell_name

        it_behaves_like 'validates option', :super_cell_name
      end

      it_behaves_like 'setted option', :scope
    end
  end

  describe 'Authentication' do
    context 'auth_code auth' do
      # You have to manually get this code before each test run
      let(:auth_code) { '63c02f63651f2a594ecdcc6e3a80b71e15c76849ef4dacca135063762470d813' }

      let(:options) {{
          client_id: client_id,
          client_secret: client_secret,
          scope: 'write:drop',
          super_cell_name: super_cell_name,
          redirect_uri: 'urn:ietf:wg:oauth:2.0:oob',
          grant_type: 'authorization_code'
        }
      }

      let(:dropigee) { described_class.new options }

      before { dropigee.set_code(auth_code).auth }

      context 'with correct credentials' do
        it 'makes authorization' do
          expect(dropigee.authorized?).to eq true
        end
      end

      context 'with wrong credentials' do
        let(:auth_code) { 'wrong_code' }

        it 'throws error' do
          expect(dropigee.authorized?).to eq false
        end
      end
    end

    context 'client_credentials auth' do
      context 'with correct options' do
        let(:options) {{
            client_id: client_id,
            client_secret: client_secret,
            scope: 'write:drop',
            super_cell_name: super_cell_name,
            redirect_uri: 'urn:ietf:wg:oauth:2.0:oob',
            grant_type: 'client_credentials'
          }
        }

        let(:dropigee) { described_class.new options }

        it 'authenticates' do
          expect(dropigee.authorized?).to eq true
        end
      end
    end
  end

  describe 'Drops' do
    let(:options) {{
        client_id: client_id,
        client_secret: client_secret,
        scope: 'write:drop',
        super_cell_name: super_cell_name,
        redirect_uri: 'urn:ietf:wg:oauth:2.0:oob',
        grant_type: 'client_credentials'
      }
    }

    describe 'drops object' do
      subject { described_class.new(options).drops }

      it 'is a Drops object' do
        expect(subject).to be_a Dropigee::Drops
      end
    end

    describe 'GET Index Drops' do
      subject { described_class.new(options).drops }

      context 'with valid super_cell_name' do
        it 'returns drops info' do
          data = subject.index

          expect(data).to include 'drops'
          expect(data['drops']).to be_an Array
        end

        context 'with q param' do
          it 'returns correct drops info if there are matches' do
            data = subject.index({q: 'dropigee'})

            expect(data['drops']).to_not be_empty
          end

          it 'returns empty result if there are no matches' do
            data = subject.index({q: 'wrong_name'})

            expect(data['drops']).to be_empty
          end
        end
      end

      context 'with invalid super_cell_name' do
        subject do
          described_class.new(options)
            .supercell('wrong_name')
            .drops.index
        end

        it 'returns error response code' do
          expect { subject }.to raise_error OAuth2::Error
        end
      end
    end

    describe 'GET Show a Drop' do
      def drop_show
        described_class.new(options)
          .drops.show(drop_id)
      end

      context 'without drop_id' do
        let(:drop_id) {''}

        it 'throws an error' do
          expect{drop_show}.to raise_error ArgumentError
        end
      end

      context 'with drop_id' do
        it 'returns drop info' do
          data = drop_show

          expect(data).to be_a Hash
          expect(data['drop']).to be_a Hash
          expect(data['drop']['id']).to eq drop_id
          expect(data['drop'].keys).to include(
            'id',
            'private',
            'versioned',
            'version_id',
            'name',
            'size',
            'etag',
            'mime_type',
            'download_url',
            'preview_url',
            'source',
            'created_at'
          )
        end
      end
    end

    describe 'POST Create a Drop' do
      subject { described_class.new(options).drops }

      def drop_create
        subject.create file, file_params
      end

      let(:file_params) {{
          private: true,
          versioned: true
        }
      }

      context 'without file' do
        let(:file) { nil }

        it 'throws an error' do
          expect{drop_create}.to raise_error ArgumentError
        end
      end

      context 'with file' do
        #TODO delete file after upload and test passing
        it 'uploads file' do
          result = drop_create

          expect(result).to be_a Hash
          expect(result['drop']).to be_a Hash

          drop = result['drop']

          @new_drop_id = result['drop']['id']
          expect(@new_drop_id).to be_a String
          expect(@new_drop_id.length).to be > 0

          #TODO move this include() array to separate var in two places
          expect(result['drop'].keys).to include(
            'id',
            'private',
            'versioned',
            'version_id',
            'name',
            'size',
            'etag',
            'mime_type',
            'download_url',
            'preview_url',
            'source',
            'created_at'
          )

          expect(drop['private']).to eq file_params[:private]
          expect(drop['versioned']).to eq file_params[:versioned]
        end
      end
    end

    describe 'PUT/PATCH Update a Drop' do
      let(:old_params) {{
          private: false,
          versioned: false
        }
      }
      let(:new_params) {{
          private: true,
          versioned: true
        }
      }

      subject { described_class.new(options).drops }

      context 'with wrong data' do
        it 'raise errors' do
          expect{
            subject.update( '', new_params )
          }.to raise_error ArgumentError
        end
      end

      context 'with correct data' do
        before do
          result = subject.create file, old_params
          @drop_id = result['drop']['id']
        end

        it 'returns updated object' do
          data = subject.update( @drop_id, new_params )

          expect(data).to be_a Hash
          expect(data['drop']).to be_a Hash

          drop_id = data['drop']['id']
          expect(drop_id).to be_a String
          expect(drop_id.length).to be > 0

          #TODO move this include() array to separate var in two places
          expect(data['drop'].keys).to include(
            'id',
            'private',
            'versioned',
            'version_id',
            'name',
            'size',
            'etag',
            'mime_type',
            'download_url',
            'preview_url',
            'source',
            'created_at'
          )

          expect(data['drop']['private']).to eq new_params[:private]
          expect(data['drop']['private']).to_not eq old_params[:private]

          expect(data['drop']['versioned']).to eq new_params[:versioned]
          expect(data['drop']['versioned']).to_not eq old_params[:versioned]
        end
      end
    end

    describe 'DELETE Destroy a Drop' do
      subject { described_class.new(options).drops }

      context 'with correct data' do
        before do
          result = subject.create file, {}
          @drop_id = result['drop']['id']
        end

        it 'deletes drop' do
          subject.destroy(@drop_id)

          expect { subject.show(@drop_id)}.to raise_error OAuth2::Error
        end
      end

      context 'with wrong data' do
        it 'raises error' do
          expect {
            subject.destroy('blahblah')
          }.to raise_error OAuth2::Error
        end
      end

      context 'with missing data' do
        it 'raises error' do
          expect {
            subject.destroy('')
          }.to raise_error ArgumentError
        end
      end
    end

  end

  describe 'Pagination'
end
