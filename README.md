# Dropigee

This gem provides access to [Dropigee](https://dropigee.com) API

Information about API itself can be found here: https://dropigee.com/developers

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'dropigee'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install dropigee

## Usage
### Creating an instance
```ruby
  options = {
    client_id: 'your_client_id',
    client_secret: 'your_client_secret',
    scope: 'write:drop',
    super_cell_name: 'your_super_cell_name',
    redirect_url: 'your_redirect_url',
    grant_type: 'desired_grant_type',
    skip_auth: false
  }
  dropigee = Dropigee.new(options)
```
dropigee will try to authenticate instance upon creation.
If you don't want to authenticate right away, set `skip_auth` option to `true`.
You will be able to authenticate anytime later by calling `#auth` method.

### Authentication
#### Web Application (authentication_code)
If you are using Dropigee in your web application, you will have to ask for user's permission to give an access to your application.

Provide `grant_type: 'authorization_code` param while creating an instance.
Use
    `url = dropigee.auth_code_url`
to generate url for user.

Set code, received from user to redeem auth token like this:
```ruby
  dropigee.set_code(auth_code)
  auth_result = dropigee.auth
```
`#auth` method return boolean value which tells you whether or not authentication was successful.

#### Non-Web Application
Use non-web application auth if there is no third-party between your application and Dropigee.

Provide `grant_type: 'client_credentials'` param while creating an instance to use non-web application authentication.
Creating instance of Dropigee will by default try to authenticate app with provided credentials.
```ruby
  dropigee = Dropigee.new(options)
  # To check auth result
  dropigee.authorized?
```


### Dropigee Helper Methods
#### Get options
`dropigee.get_options` method is used to retreive a hash of options

#### Set options
`dropigee.set_options(options)` method is used to update an existing hash of options

#### Set code
`dropigee.set_code(code)` method is just a handy shotrcut for `dropigee.set_options(code: code)`
Use this method in web application auth flow when you want to set auth code pvodided to you by user

#### authorized?
Call `dropigee.authorized?` anytime to check if your `dropigee` instance is authorized and ready to perform drop calls.


### General usage
After completing authentication you may start querying resourses.

General approach consists of creating collection object and calling collection method

It generally looks like this:

```ruby
  collection = dropigee.collection_name

  result = collection.method(params)
```
Note, that `collection` object will utilize options, specified in `dropigee` object. We are mainly talking about `super_cell_name` param.

If you haven't specified supercell during `dropigee` initialization or you want to use different supercell, you have two options:

You can either change supercell upon collection initialization:
```ruby
  collection = dropigee.supercell('supercell_name').collection_name

  result = collection.method(params)
```

or set supercell later, by calling corresponding `collection` method
```ruby
  collection = dropigee.collection_name

  result = collection.supercell('supercell_name').method(params)
```

### Drops collection
For more information about available options and return values for each method, visit [Dropigee developers site](https://www.dropigee.com/developers/drops/)

#### Creating drops object
```ruby
  drops = dropigee.drops
```

#### Drop index
`drops.index(options={})` method is used to retreive information about drops avilable in given super cell.
`options` param is an optional hash with search condition. Refer Dropigee developers documentation for more information.

#### Drop show
`drops.show(drop_id)` method is used to retreive information about `drop` with provided `id`.

#### Drop create
`drops.drop_create(file, options={})` method is used to upload new file to Dropigee.
`file` param is required and is expected to be an instance of a [File class](http://ruby-doc.org/core-2.2.0/File.html).
`options` is a hash of file parameters. Defaults to empty hash.

#### Drop update
`drops.update(drop_id, options)` method is used to update an existing drop.
`drop_id` param is required and specifies an id of existing drop you wish to update.
`options` param is required as well and is expected to be a hash with keys equal to allowed drop options and values equal to values you want to set.

#### Drop destroy
`drops.destroy(drop_id)` method is used to delete an existing drop.
drop_id` param is required and specifies an id of existing drop you wish to delete.

## Testing
Tests for Dropigee gem are written using [RSpec gem](https://github.com/rspec)

#### Warning
Dropigee specs does not rely on mocking and perform real calls to Dropigee server.
In order for specs to do so, you have to provide your working Dropigee credentials.
**Be sure not to commit your credentials to VCS!**

Make sure not to use your primary Dropigee application for testing purposes.
Running specs may not clean all files it creates properly.
You've been warned.

### Running specs
You'll find this block at the beginning of `spec/dropigee.rb` file:
```ruby
  let(:client_id) { 'client_id_here' }
  let(:client_secret) { 'client_secret_here' }
  let(:super_cell_name) { 'sc_name_here'}
  let(:drop_id) { 'drop_id_of_sc_above' }
```
Fill those lets with valid credentials in order to run specs successfully.

### Testing Web-flow authentication
In order to test web-auth flow, you'll have to provide specs with valid auth code.
Find the following let and specify working auth_code
```ruby
  let(:auth_code) { 'auth_code_here' }
```
Make sure to generate working code everytime you want to run this particular spec, as this code will expire after first use.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/dropigee.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

