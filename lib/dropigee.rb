require "dropigee/version"
require 'dropigee/drops'
require 'rest-client'
require 'oauth2'

class Dropigee
  HEADERS = { 'Accept' => 'application/vnd.dropigee.v1+json' }

  OAUTH_URL = 'https://www.dropigee.com/oauth/authorize/'
  OAUTH_TOKEN_URL = 'https://www.dropigee.com/oauth/token'

  DEFAULT_OPTIONS = {
    grant_type: 'client_credentials',
    scope: 'read:drop'
  }

  def initialize(options)
    @is_authorized = false

    @options = DEFAULT_OPTIONS.merge options

    validate! @options

    @client = OAuth2::Client.new(
      @options[:client_id],
      @options[:client_secret],
      token_url: OAUTH_TOKEN_URL,
      authorize_url: OAUTH_URL
    )

    auth unless @options[:skip_auth]
  end

  def authorized?
    @is_authorized
  end

  def get_options
    @options
  end

  def set_options(options)
    @options.merge! options

    self
  end

  def supercell(name)
    @options.merge!({super_cell_name: name})

    self
  end

  def drops
    Dropigee::Drops.new(@token, @options[:super_cell_name])
  end

  def set_code(code)
    @options.merge!({code: code})

    self
  end

  def auth
    case @options[:grant_type]
    when 'authorization_code'
      if @options[:code]
        return auth_with_authorization_code(@options[:code])
      end
    when 'client_credentials'
      return auth_with_client_credentials
    else
      raise ArgumentError('Grant type is not provided')
    end
  end

  def auth_code_url
    url = @client.auth_code.authorize_url(
      scope: @options[:scope],
      redirect_uri: @options[:redirect_uri]
    )

    url
  end

  def auth_with_authorization_code(auth_code)
    return perform_auth_call do
      @token = @client.auth_code.get_token(
        auth_code,
        redirect_uri: @options[:redirect_uri],
        headers: HEADERS
      )
    end
  end

  def auth_with_client_credentials
    return perform_auth_call do
      @token = @client.client_credentials.get_token(
        scope: @options[:scope],
        headers: HEADERS
      )
    end
  end

  protected

  def validate!(options)
    %w(client_id client_secret super_cell_name).each do |field|
      field_symbol = field.to_sym

      if @options[field_symbol].nil? || @options[field_symbol].empty?
        throw ArgumentError.new("Wrong #{field} provided")
      end
    end
  end

  def perform_auth_call
    @token = nil

    begin
      yield

      @is_authorized = true
    rescue OAuth2::Error => e
      @is_authorized = false
    end

    return authorized?
  end

  def oauth_token_url
    OAUTH_TOKEN_URL
  end
end
