class Dropigee
  class Drops

    DROPS_URL = 'https://api.dropigee.com/supercells/{super_cell_name}/drops/'
    DROP_URL = 'https://api.dropigee.com/supercells/{super_cell_name}/drops/{drop_id}/'

    UPLOAD_FILE_TO_S3_URL = 'https://cdn.supercell.io/'

    def initialize(token, super_cell_name)
      #TODO validation of those two
      @token = token
      @super_cell_name = super_cell_name
    end

    def supercell(name)
      @supercell = name

      self
    end

    def index(options={})
      response = @token.get(
        drops_url,
        params: options
      )

      parse_response response
    end

    def show(drop_id)
      raise ArgumentError.new('drop_id is incorrect') if drop_id.empty?

      response = @token.get(
        drop_url(drop_id)
      )

      parse_response response
    end

    # options:
    #   private: true,
    #   versioned: true
    def create(file, options={})
      raise ArgumentError.new('file is empty') if file.nil?

      response = @token.post(
        drops_url,
        body: options
      )

      #TODO check for errors here after first request
      upload_params = JSON.parse(response.body)['parameters']

      file_post_result = RestClient.post(
        upload_file_to_s3_url,
        upload_params.merge({file: file})
      )

      parse_response file_post_result
    end

    def update(drop_id, options)
      raise ArgumentError.new('drop_id is incorrect') if drop_id.empty?

      response = @token.put(
        drop_url(drop_id),
        body: options
      )

      parse_response response
    end

    def destroy(drop_id)
      raise ArgumentError.new('drop_id is incorrect') if drop_id.empty?

      @token.delete(
        drop_url(drop_id)
      )
    end


    protected

    def drops_url
      construct_url( DROPS_URL, {
      'super_cell_name' => get_super_cell
      })
    end

    def drop_url(drop_id)
      construct_url( DROP_URL, {
      'super_cell_name' => get_super_cell,
      'drop_id' => drop_id
      })
    end

    def upload_file_to_s3_url
      UPLOAD_FILE_TO_S3_URL
    end

    def construct_url(url_template, tokens={})
      url = url_template.clone

      tokens.each do |token, value|
        url.sub! "{#{token}}", value
      end

      url
    end

    def parse_response(response)
      return JSON.parse(response) if response.is_a? String

      JSON.parse(response.body)
    end

    def get_super_cell
      @super_cell_name
    end
  end
end
